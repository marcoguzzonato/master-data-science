from flask import Flask
app = Flask("data_science")

@app.route('/')
def hello_world():
    return "Hello World"

if __name__ == "__main__":
    port = 5000
    host = '0.0.0.0'
    app.run(host=host, port=port)
